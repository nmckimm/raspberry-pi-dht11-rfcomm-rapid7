"""
COMP47520 - Assignment 2
Stephen Colfer
Neil McKimm

Server script for connection via bluetooth to collect and send data from the DHT11
Temperature and Humidty sensor.

Note: May require 'python' command to run instead of 'python3'
"""

import Adafruit_DHT
import os
import time
from time import sleep
from datetime import datetime as dt
import bluetooth
from r7insight import R7InsightHandler
import logging

log = logging.getLogger('Pi')
log.setLevel(logging.INFO)
test = R7InsightHandler('f3bfefcd-58cd-4365-afac-4cc5d8385745', 'eu')

#Bluetooth Connection - port 1 not working
server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )

port = 3
server_sock.bind(("",port))
server_sock.listen(3)

client_sock,address = server_sock.accept()
print("Accepted connection from ",address)

# Assigning variables to the DHT11 sensor and its data GPIO pin
sensor = 11
pin = 4
i = 0


def alertFunct(humidity, temperature):
    """
    Function that takes input of temperature and humidty and returns an alert string based on the values

    """


    tempAlert = ""
    humAlert = ""
    humidity, temperature = float(humidity), float(temperature)
    if temperature > 28:
        tempAlert += "It is very hot"
    elif 28 > temperature >= 25:
        tempAlert += "It is warm"
    elif temperature < 25:
        tempAlert += "Temperature is normal"
        
    if 70 > humidity > 50:
        humAlert += "humidity is high"
    elif 101 > humidity > 69:
        humAlert += "humidity is very high"  

    # Processing alert to allow for alert concatenation
    if len(humAlert) > 1 and len(tempAlert) >1):
        alert = tempAlert + " and " + humAlert
    elif len(humAlert) > 1 and len(tempAlert) < 1:
        alert = humAlert
    else:
        alert = tempAlert
    return alert


# Loop to check temperature and humidity values a certain amount of iterations
for i in range(0, 100):

    # Client data
    clientData = client_sock.recv(2048)
    print("received [%s]" % clientData)
    clientData = clientData[1:len(clientData)-1]
    timeCli, sensorCli, temperatureCli, humidityCli = clientData.split(",")

    # Server Data
    humiditySer, temperatureSer = Adafruit_DHT.read_retry(sensor, pin)

    # Log Server sensor data
    now = dt.now()
    current_date = dt.date(now)
    current_time = dt.time(now)
    log.info(str(now) + "  SensorID=Neil" + "," + "Humidity={} %".format(humiditySer) + "," + "Temperature={} C".format(temperatureSer) +"\n")

    # Alert Server sensor data
    log.addHandler(test)
    alert = alertFunct(humiditySer, temperatureSer)
    log.warn(alert)

    # Log Client sensor data
    log.info(str(timeCli) + "  SensorID=Stephen" + "," + "Humidity={} %".format(float(humidityCli)) + "," + "Temperature={} C".format(float(temperatureCli)) +"\n")
  
    # Alert Client sensor data
    log.addHandler(test)
    alert2 = alertFunct(humidityCli, temperatureCli)
    log.warn(alert2)

    time.sleep(5)


client_sock.close()
server_sock.close()

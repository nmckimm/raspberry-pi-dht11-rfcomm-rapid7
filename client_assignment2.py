"""
COMP47520 - Assignment 2
Stephen Colfer
Neil McKimm

Client script for connection via bluetooth to collect and send data from the DHT11 
Temperature and Humidty sensor. 
 
Note: May require 'python' command to run instead of 'python3'
"""


import Adafruit_DHT as Adafruit
import time
import datetime
import bluetooth



#Bluetooth scan 
target_address = "B8:27:EB:09:9C:9D"

nearby_devices = bluetooth.discover_devices(lookup_names = True)
print("%d devices found \n" % len(nearby_devices))

for a,n in nearby_devices:
    print("  %s - %s" % (a,n))

for bdaddr in nearby_devices:
    if target_address == bdaddr:
        print("Device found")
    else:
        print("could not find target bluetooth device nearby")

#Connection
port = 3
sock = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
sock.connect((target_address, port))


#Retrieve and send sensor data
pin = 4
sensor = Adafruit.DHT11

#100 iterations
for i in range(0,100):

    try:
        humidity, temperature = Adafruit.read_retry(sensor, pin)
    except:
        print("unable to gain reading from sensor")


    if humidity is not None and temperature is not None:
        s = str((datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), "Stephen", temperature, humidity)) 
        print(s)
        sock.send(s)

    time.sleep(5)

sock.close

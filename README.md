[IoT_Project_1_-_Report.pdf](/uploads/f8779a4bcd9d84c3efab8058391e262d/IoT_Project_1_-_Report.pdf)


COMP47520 Programming for IoT

Team: Neil McKimm, Stephen Colfer

	
						
UCD School of Computer Science
University College Dublin 
November, 2018 



Note: Full report with included figures is attached






Introduction
When first thinking about programming for IoT one might think the majority of work creating a system lies within the capturing of sensor data, when in reality what makes a system so effective and robust is networking quality. In this project we set out to create a small network of two Raspberry Pis connected via bluetooth to send across sensor data from the DHT11 temperature and humidity sensor and log to the cloud. One Pi acted as a client, the other as a server. The client pi would only get the data from its sensor and send it to the server pi, while the server would retrieve its own sensor data as well as the clients data and upload both to the Rapid7 InsightOps cloud.


Build Steps
Connect the DHT11 to the correct GPIO pins
Obtain the DHT11 sensor data locally onto each Pi
Connect and send basic message via bluetooth
Send sensor data over bluetooth
Sent the correct format data across bluetooth
Connect to Rapid7
Send sensor data to Rapid7
Create widgets and alerts for Rapid7


Instructions for Use

Client.py
Run with python or python3, this script should be run on the client Pi (currently hard coded with the Mac address of our server Pii). The client scans nearby bluetooth devices and attempt to connect to the specific server pi. Upon successful connection the client sends a string of the format ('2018-11-13 11:19:50.769308', 'Stephen', 26.0, 55.0) every 5 seconds. This is a tuple including the timestamp of when the values were taken, name of the sensor, the temperature value and the humidity value. These values are parsed by the server.

Server.py
Run with python3, this script should be run on the server Pi. Must have internet connection in order to access Rapid7. This connection is first established then the server listens for a bluetooth connection. Once a connection is received it begins collecting its own sensor data as well as parsing the client data. The server and client data are then sent to the cloud as a string of the format :		 	 	 	
YYYY-MM-DD HH:MM:SS.ms SensorID=<studentName>, Temperature=xx.x, Humidity=x 
Additionally, alerts based on the constraints outlined below are sent for each sensor.

InsightOps
The account on InsightOps includes tags for alerts based on the sensor data. Also included are charts to display value trends for each sensor.


Problems 
Main issue involved connecting to the Rapid7 service. This appeared to only work correctly when connected to the COMP47520 wireless router during the lab sessions. Otherwise the Pi didn’t seem to be able to access the internet despite being connected to the network. 
Connecting the Raspberry Pis by bluetooth: Using the RFCOMM functionality we realised (from trial and error) that port 1 did not work correctly. Thus we used port 3 in the final build. Other issues involved unrecognisable devices, i.e. during pairing they were recognised as headsets with limited data transfer options.
Dates on the Pis are technically incorrect: Despite attempts to reset the time of the machines, we are unable during the time allocated while still allowing time to complete the task at hand. Therefore we focused in getting the correct implementation of the system first. However in a real life system this would be of the utmost importance to have the correct timestamps for data.


Improvements
This system is based simply on 2 data points connected to the cloud which can be upscaled to a larger system with more nodes such as a building with data points in each room. 
Increased error handling: To proof inputs and connections across the network, by created alerts for null values. This would notify the user of a failed node.
A backup server could be made available where in the case of one failing, the backup server will activate to take over, ensuring no drop in data values. Even a client/slave could be set to automatically take over as a server in the event of a lost connection. This would make the system extremely robust if the client nodes had the resources available to do this. There would be a trade-off with build cost, however this would allow for self-healing of the network which is very desirable in systems like this.
Higher quality sensors or multiple sensors in a room for an average could be added to improve the accuracy of the reading. 
As mentioned, the Pis cannot connect to the internet on any other WiFi network than the COMP47520 network. This is obviously an issue that will need to be addressed to improve the system.
